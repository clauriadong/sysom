import ProForm, { ProFormText, ProFormSelect } from '@ant-design/pro-form';
import { Button } from 'antd';
import { useRequest, useIntl, FormattedMessage } from 'umi';
import { useEffect, useState, useRef } from 'react';
import ProCard from '@ant-design/pro-card';
import { postTask } from '../service'
import { getHost } from '@/pages/host/service'

export default (props) => {
    const intl = useIntl();
    const formRef = useRef();
    const [hostList, setHostList] = useState([]);
    const { loading, error, run } = useRequest(postTask, {
        manual: true,
        onSuccess: (result, params) => {
            props?.onSuccess?.(result, params);
        },
    });

    useEffect(async ()=> {
        const { data } = await getHost()
        let list = []
        data.map((item) => {
            list.push({"value": item.ip, "label": item.ip})
        })
        setHostList(list)
    }, [])

    const onChange = (value) => {
        if (value) {
            formRef?.current?.setFieldsValue({'instance': value})
        }
      };
    const onSearch = (value) => {
        if (value) {
            formRef?.current?.setFieldsValue({'instance': value})
        }
    };

    return (

        <ProCard>
            <ProForm
                formRef={formRef}
                onFinish={async (values) => { run(values) }}
                submitter={{
                    submitButtonProps: {
                        style: {
                            display: 'none',
                        },
                    },
                    resetButtonProps: {
                        style: {
                            display: 'none',
                        },
                    },
                }}
                layout={"horizontal"}
                autoFocusFirstInput
            >
                <ProFormText
                    name={"service_name"}
                    initialValue={"ossre"}
                    hidden={true}
                />
                <ProForm.Group>
                    <ProFormSelect
                        name={"instance"}
                        width="md"
                        label={intl.formatMessage({
                            id: 'pages.diagnose.instanceIP',
                            defaultMessage: 'Instance IP',
                        })}
                        rules={[
                            { required: true, message: 'Please input your instance!' },
                        ]}
                        options={hostList}
                        fieldProps={{
                            "allowClear": true,
                            "showSearch": true,
                            "onChange": onChange,
                            "onSearch": onSearch
                        }}
                        placeholder="Please select or input instance!"
                    />
                    <Button type="primary" htmlType="submit" loading={loading}><FormattedMessage id="pages.diagnose.startdiagnosis" defaultMessage="Start diagnosis" /></Button>
                </ProForm.Group>
            </ProForm>
        </ProCard>
    )
}
