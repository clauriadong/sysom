import os
import re
from clogger import logger
from django.conf import settings

from apps.hotfix.models import HotfixModel, OSTypeModel, KernelVersionModel
from cec_base.producer import dispatch_producer, Producer

"""
Function class
This class contains the support/tool function
"""
class FunctionClass():

    def __init__(self):
        self.cec = dispatch_producer(settings.SYSOM_CEC_URL)

    def delete_document(self, doc_path, doc_name):
        document = os.path.join(doc_path, doc_name)
        if os.path.exists(document):
            try:
                os.remove(document)
            except Exception as e:
                logger.error(str(e))
                return None
        else:
            return None    

    def query_formal_hotfix_by_parameters(self, created_time, kernel_version, patch_file, hotfix_name):
        if created_time is not None and len(created_time) <= 0:
            created_time=None
        if kernel_version is not None and len(kernel_version) == 0:
            kernel_version = None
        if patch_file is not None and len(patch_file) == 0:
            patch_file=None
        if hotfix_name is not None and len(hotfix_name) == 0:
            hotfix_name=None
        objects = HotfixModel.objects.all().filter(formal=1)
        if created_time is not None:
            objects = objects.filter(created_at__lt=created_time)
        if kernel_version is not None:
            objects = objects.filter(kernel_version=kernel_version)
        if patch_file is not None:
            objects = objects.filter(patch_file=patch_file)
        if hotfix_name is not None:
            objects = objects.filter(hotfix_name=hotfix_name)
        return objects

    def get_info_from_version(self, kernel_version, info="os_type"):
        try:
            version_object = KernelVersionModel.objects.all().filter(kernel_version=kernel_version).first()
            if version_object is None:
                logger.error("query kernel version from record failed")
                return None
            if info == "os_type":
                return version_object.os_type
            if info == "source":
                return version_object.source
            if info == "devel_link":
                return version_object.devel_link
            if info == "debuginfo_link":
                return version_object.debuginfo_link
        except Exception as e:
            logger.error(str(e))
            return None        

    def get_sourcerepo_of_os(self, os_type):
        try:
            os_object = OSTypeModel.objects.all().filter(os_type=os_type).first()
            if os_object is None:
                return None
            return os_object.source_repo
        except Exception as e:
            logger.error(str(e))
            return None
      
    def get_image_of_os(self, os_type):
        try:
            os_object = OSTypeModel.objects.all().filter(os_type=os_type).first()
            if os_object is None:
                return None
            return os_object.image
        except Exception as e:
            logger.exception(e)
            return None
    
    def get_src_pkg_mark_of_os(self, os_type):
        try:
            os_object = OSTypeModel.objects.all().filter(os_type=os_type).first()
            if os_object is None:
                return None
            return os_object.src_pkg_mark
        except Exception as e:
            logger.exception(e)
            return None

    # building status and formal is set to be 0 when creating a hotfix
    def create_hotfix_object_to_database(self, os_type, kernel_version, hotfix_name, patch_file, patch_path, hotfix_necessary, hotfix_risk, 
    log_file, arch):
        res = HotfixModel.objects.create(
            kernel_version = kernel_version,
            os_type=os_type,
            patch_file = patch_file,
            hotfix_name = hotfix_name,
            patch_path = patch_path,
            building_status = 0,
            hotfix_necessary = 0,
            hotfix_risk = 2,
            formal = 0,
            log_file = log_file,
            arch = arch
        )
        return res

    def create_message_to_cec(self, **kwargs):
        customize = kwargs['customize']
        cec_topic = kwargs['cec_topic']
        os_type = kwargs['os_type']
        hotfix_id = kwargs['hotfix_id']
        kernel_version= kwargs['kernel_version']
        patch_file = kwargs['patch_file']
        hotfix_name = kwargs['hotfix_name']
        patch_path = kwargs['patch_path']
        arch = kwargs['arch']
        log_file = kwargs['log_file']
        try:
            if not customize:
                if re.search('anolis', os_type):
                    self.cec.produce(cec_topic, {
                            "hotfix_id" : hotfix_id,
                            "kernel_version" : kernel_version,
                            "patch_name" : patch_file,
                            "hotfix_name" : hotfix_name,
                            "patch_path" : patch_path,
                            "arch": arch,
                            "log_file" : log_file,
                            "os_type" : os_type,
                            "git_repo": "git@gitee.com:anolis/cloud-kernel.git",
                            "customize": 0
                        })
                return True
            else:
                # this is customize kernel
                source_repo = kwargs['source_repo']
                source = kwargs['source']
                devel_link = kwargs['devel_link']
                debuginfo_link = kwargs['debuginfo_link']
                image = kwargs['image']
                is_src_package = kwargs["is_src_package"]
                self.cec.produce(cec_topic, {
                    "hotfix_id" : hotfix_id,
                    "kernel_version" : kernel_version,
                    "hotfix_name" : hotfix_name,
                    "patch_name" : patch_file,
                    "patch_path" : patch_path,
                    "arch": arch,
                    "log_file" : log_file,
                    "os_type" : os_type,
                    "customize": 1,
                    "src_repo": source_repo,
                    "src_origin": source,
                    "devel_link": devel_link,
                    "debuginfo_link": debuginfo_link,
                    "image": image,
                    "is_src_package": is_src_package
                })
            return True
        except Exception as e:
            logger.exception(e)
            return False

    def get_hotfix_object_by_id(self, hotfix_id):
        try:
            hotfix_object = HotfixModel.objects.all().filter(id=hotfix_id).first()
            return hotfix_object
        except Exception as e:
            logger.error(str(e))
            return None

    def get_host_list(self, kernel_version):
        host_url = settings.HOST_URL
        res = requests.get(host_url)
        if res.status_code == 200:
            res = res.json()
            data = res['data']
            host_list = []
            for each_host in data:
                try:
                    if each_host['host_info']['kernel_version'] == kernel_version:
                        host_list.append(each_host["ip"])
                except Exception as e:
                    pass
            return host_list
        else:
            return None

    def dispatch_hotfix_cmd(self, ip, hotfix_name):
        machine_hotfix_path = "/root/%s" % hotfix_name
        job = default_channel_job_executor.dispatch_job(
            channel_type="ssh", 
            params={
                "instance": ip,
                "command": "rpm -ivh %s" % machine_hotfix_path,
            },
            timeout=1000,
            auto_retry=False
        )
        channel_result = job.execute()
        # the result is the output of executing the cmd
        # result = channel_result.result
        if channel_result.code != 1:
            return False
        return True

    """
    Args : Given kernel version and the hotfix path
    Return : the number of instance successfully execute the dispatch hotfix and installed hotfix
    """
    def deploy_hotfix_to_machine(self, kernel_version, hotfix_path):
        try:
            hotfix_list = self.get_host_list(kernel_version)
            hotfix_name = hotfix_path.split("/")[-1]
            """dispatch the hotfix package to one machine"""
            res = default_channel_job_executor.dispatch_file_job(params={
                "local_path": hotfix_path,
                "remote_path": "/root/%s" % hotfix_name,
                "instances": hotfix_list
            }).execute().result
            # the res is string
            res = json.loads(res)
            for each_instance in res:
                if not each_instance["success"]:
                    logger.error("Instance ip: {} dispatch hotfix:{} failed".format(each_instance["instance"], hotfix_path))
            hotfix_name = hotfix_path.split("/")[-1]

            success_num = 0
            for each_host in hotfix_list:
                if self.dispatch_hotfix_cmd(each_host, hotfix_name):
                    success_num += 1

            return success_num

        except Exception as e:
            logger.error(str(e))
            return None
        
        

